import {createStackNavigator, createAppContainer} from 'react-navigation';
import HomeScreen from './component/HomeScreen.js'
import ProfileScreen from './component/ProfileScreen.js'
const MainNavigator = createStackNavigator({
    Home: {screen: HomeScreen},
    Profile: {screen: ProfileScreen},
});

const App = createAppContainer(MainNavigator);

export default App;