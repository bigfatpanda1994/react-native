import {Button, Text, View, StyleSheet} from "react-native";
import React from "react";

class HomeScreen extends React.Component {
    static navigationOptions = {
        title: 'Home',
    };

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={{flex: 1}}>
                <View style={{flex: 1, backgroundColor: 'powderblue'}}>
                    <Text style={styles.titleText}>
                            Ha noi{'\n'}
                        <Text style={styles.baseText}>
                            Mostly Cloudy
                        </Text>
                    </Text>
                </View>
                <View style={{flex: 2, backgroundColor: 'skyblue'}}>
                    <Text style={styles.hiperText}>
                        29&#176;
                    </Text>
                </View>
                <View style={{flex: 3, backgroundColor: 'steelblue'}}/>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    baseText: {
        fontSize: 25
    },
    titleText: {
        fontSize: 45,
        //fontWeight: 'bold',
        color: '#ffffff',
        textAlign: 'center',
        padding: 20
    },
    hiperText: {
        fontSize: 100,
        fontWeight: '200',
        color: '#ffffff',
        textAlign: 'center',
        padding: 20
    },
});


export default HomeScreen;